import React, { Component, Fragment } from "react";
import { Provider } from "react-redux";
import Home from "./pages/home";

import configureStore from "./redux/_store/store";
const store = configureStore();
export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Fragment>
          <Home />
        </Fragment>
      </Provider>
    );
  }
}
