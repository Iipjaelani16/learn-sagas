import React, { Component } from "react";
import { connect } from "react-redux";
import { setLoadUsers } from "../redux/_actions/userActions";
import { Avatar } from "@material-ui/core";
import { setConstLoad } from "../redux/_actions/constActions";
import { setLoadAddUsers } from "../redux/_actions/addusers";

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      a: null,
    };
  }
  componentDidMount() {
    this.props.setLoadUsers();
  }

  Adddcoba = () => {
    const data = {
      name: "dsds",
    };
    console.log(data);

    this.props.setLoadAddUsers(data);
  };
  render() {
    const { users, isLoading } = this.props.nextUsers;
    if (isLoading) {
      return (
        <div>
          <p>Loading</p>
        </div>
      );
    }
    return (
      <div>
        <div>
          <p onClick={() => this.props.setConstLoad()}>tes</p>
        </div>
        <div>
          <p onClick={() => this.Adddcoba()}>add</p>
        </div>
        <div>
          <p>{this.props.tess}</p>
        </div>
        {users.map((data) => (
          <div>
            <div>
              <Avatar alt="Remy Sharp" src={data.url} />
            </div>
            <p>{data.title}</p>
          </div>
        ))}
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  // console.log(state, "----isi state");
  return {
    nextUsers: state.nextUsers,
    tess: state.tes,
    nextAdd: state.nextAdd,
  };
};

const mapDispatchToProps = (dispatch) => ({
  setLoadUsers: () => dispatch(setLoadUsers()),
  setConstLoad: () => dispatch(setConstLoad()),
  setLoadAddUsers: () => dispatch(setLoadAddUsers()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Home);
