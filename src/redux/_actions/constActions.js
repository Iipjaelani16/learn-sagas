import { CONST } from "../constants/constans";

const setConstLoad = () => ({
  type: CONST.LOADING,
});

const setFullConst = (constant) => ({
  type: CONST.FULL_FIELD,
  constant,
});

const setErrorCOnst = (error) => ({
  type: CONST.REJECTED,
  error,
});

export { setConstLoad, setFullConst, setErrorCOnst };
