import { GET_USERS } from "../constants/constans";

const setLoadUsers = () => ({
  type: GET_USERS.LOADING,
});

const setFullUsers = (users) => ({
  type: GET_USERS.FULL_FIELD,
  users,
});

const setErrorUsers = (error) => ({
  type: GET_USERS.REJECTED,
  error,
});

export { setLoadUsers, setFullUsers, setErrorUsers };
