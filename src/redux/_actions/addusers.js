import { ADD_USERS } from "../constants/constans";

const setLoadAddUsers = (data) => ({
  type: ADD_USERS.LOADING,
  data,
});

const setFullAddUsers = (data) => ({
  type: ADD_USERS.FULL_FIELD,
  data,
});

const setAddErrorUsers = (error) => ({
  type: ADD_USERS.REJECTED,
  error,
});

export { setLoadAddUsers, setFullAddUsers, setAddErrorUsers };
