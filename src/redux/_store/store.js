import { createStore, applyMiddleware, compose } from "redux";
import rootReducers from "../_reducers";
import rootSagas from "../sagas/rootsagas";
import { promise, logger } from "./middleware";
import constSagas from "../sagas/constSagas";
import AddSagas from "../sagas/addSagas";

const configureStore = () => {
  const sagaMiddleware = promise();
  const store = createStore(
    rootReducers,
    compose(
      applyMiddleware(sagaMiddleware, logger),
      window.__REDUX_DEVTOOLS_EXTENSION__ &&
        window.__REDUX_DEVTOOLS_EXTENSION__()
    )
  );
  sagaMiddleware.run(rootSagas);
  sagaMiddleware.run(AddSagas);
  sagaMiddleware.run(constSagas);

  return store;
};

export default configureStore;
