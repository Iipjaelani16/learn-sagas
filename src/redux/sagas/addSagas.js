/* eslint-disable require-yield */
import {
  call,
  put,
  takeEvery,
  takeLatest,
  select,
  race,
} from "redux-saga/effects";
import { ADD_USERS } from "../constants/constans";
import { setFullAddUsers, setAddErrorUsers } from "../_actions/addusers";

const add = (state) => state.nextAdd;

function* handleAdd() {
  console.log(" adddddddd----------------", add);
  try {
    // const tes = yield select(getUsers);
    const users = yield race(add);
    yield put(setFullAddUsers(users));
  } catch (error) {
    // error
    yield put(setAddErrorUsers(error.toString()));
  }
}

export default function* AddSagas() {
  yield takeEvery(ADD_USERS.LOADING, handleAdd);
  // console.log("Hello sagas");
}
//
