/* eslint-disable require-yield */
import { call, put, takeEvery, takeLatest, select } from "redux-saga/effects";
import { GET_USERS } from "../constants/constans";
import { Api } from "../api";
import { setFullUsers, setErrorUsers } from "../_actions/userActions";

const getUsers = (state) => state.nextUsers;

function* handleLoading() {
  try {
    // const tes = yield select(getUsers);
    const users = yield call(Api, getUsers);
    yield put(setFullUsers(users));
  } catch (error) {
    // error
    yield put(setErrorUsers(error.toString()));
  }
  // console.log("Loading", tes);
}

export default function* usersSagas() {
  yield takeEvery(GET_USERS.LOADING, handleLoading);
  // console.log("Hello sagas");
}
//
