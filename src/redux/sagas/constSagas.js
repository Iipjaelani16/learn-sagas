/* eslint-disable require-yield */
import { call, put, takeEvery, select } from "redux-saga/effects";
import { CONST } from "../constants/constans";
import { setFullConst, setErrorCOnst } from "../_actions/constActions";

const constant = (state) => state.tes;

function* handleLoading() {
  try {
    const tes = yield select(constant);
    // const cons = yield call(constant);
    yield put(setFullConst(tes));
  } catch (error) {
    // error
    yield put(setErrorCOnst(error.toString()));
  }
  // console.log("Loading", tes);
}

export default function* constSagas() {
  yield takeEvery(CONST.LOADING, handleLoading);
  // console.log("Hello sagas");
}
//
