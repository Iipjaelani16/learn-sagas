const Api = async (tes) => {
  const response = await fetch(
    "http://jsonplaceholder.typicode.com/photos?_start=0&_limit=10"
  );
  const data = await response.json();
  if (response.status > 400) {
    throw new Error(data.error);
  }
  return data;
};
export { Api };
