import { CONST } from "../constants/constans";

const tesReducers = (state = 0, action) => {
  switch (action.type) {
    case CONST.FULL_FIELD:
      return state + 1;
    default:
      return state;
  }
};
export default tesReducers;
