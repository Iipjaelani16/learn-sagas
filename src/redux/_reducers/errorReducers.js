import { GET_USERS } from "../constants/constans";
const errorReducers = (state = null, action) => {
  switch (action.type) {
    case GET_USERS.LOADING:
    case GET_USERS.FULL_FIELD:
      return null;
    case GET_USERS.REJECTED:
      return action.error;
    default:
      return state;
  }
};

export default errorReducers;
