import { combineReducers } from "redux";

import loadReducers from "./loadingReducers";
import userReducers from "./userReducers";
import errorReducers from "./errorReducers";
import AddReducers from "./addReducers";
import tesReducers from "./tesReducers";

const rootReducers = combineReducers({
  loading: loadReducers,
  error: errorReducers,
  nextUsers: userReducers,
  nextAdd: AddReducers,
  tes: tesReducers,
});

export default rootReducers;
