import { ADD_USERS } from "../constants/constans";
const initialState = {
  addSsers: [],
  error: false,
  isPost: false,
  isLoading: false,
};
const AddReducers = (state = initialState, action) => {
  console.log(action);
  switch (action.type) {
    case ADD_USERS.LOADING:
      return {
        ...state,
        isLoading: true,
        isPost: false,
      };
    case ADD_USERS.FULL_FIELD:
      return {
        ...state,
        addSsers: action.data,
        isLoading: false,
        isPost: true,
      };
    case ADD_USERS.REJECTED:
      return {
        ...state,
        error: true,
        isPost: true,
        isLoading: false,
      };
    default:
      return state;
  }
};
export default AddReducers;
