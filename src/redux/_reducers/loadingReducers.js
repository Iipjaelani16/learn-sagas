import { GET_USERS } from "../constants/constans";
const loadReducers = (state = false, action) => {
  switch (action.type) {
    case GET_USERS.LOADING:
      return true;
    case GET_USERS.FULL_FIELD:
      return false;
    case GET_USERS.REJECTED:
      return false;
    default:
      return state;
  }
};

export default loadReducers;
