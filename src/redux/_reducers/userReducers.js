import { GET_USERS } from "../constants/constans";
const initialState = {
  users: [],
  error: false,
  isPost: false,
  isLoading: false,
};
const userReducers = (state = initialState, action) => {
  switch (action.type) {
    case GET_USERS.LOADING:
      return {
        ...state,
        isLoading: true,
        isPost: false,
      };
    case GET_USERS.FULL_FIELD:
      return {
        ...state,
        users: action.users,
        isLoading: false,
        isPost: true,
      };
    case GET_USERS.REJECTED:
      return {
        ...state,
        error: true,
        isPost: true,
        isLoading: false,
      };
    default:
      return state;
  }
};
export default userReducers;
